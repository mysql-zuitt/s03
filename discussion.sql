-- [Section] INSERTING Records/Row
-- Syntax:
	-- INSERT INTO <table_name> (columns) VALUES (values);

INSERT INTO artists (name) VALUES ("Rivermaya");
INSERT INTO artists (name) VALUES ("Blackpink");
INSERT INTO artists (name) VALUES ("Taylor Swift");
INSERT INTO artists (name) VALUES ("New Jeans");
INSERT INTO artists (name) VALUES ("Bamboo");

INSERT INTO albums (album_title,datetime_released,artist_id) VALUES ("Trip", "1996-01-01", 1);
INSERT INTO albums (album_title,datetime_released,artist_id) VALUES ("Born Pink", "2022-09-16", 2);
INSERT INTO albums (album_title,datetime_released,artist_id) VALUES ("The Album", "2020-10-02", 2);
INSERT INTO albums (album_title,datetime_released,artist_id) VALUES ("Midnights", "2020-10-21", 3);
INSERT INTO albums (album_title,datetime_released,artist_id) VALUES ("New Jeans", "2022-08-01", 4);
INSERT INTO albums (album_title,datetime_released,artist_id) VALUES ("Leaves", "2023-01-01", 5);
INSERT INTO albums (album_title,datetime_released,artist_id) VALUES ("Panda", "2023-02-02", 5);


INSERT INTO songs (song_name,length,genre,album_id)
	VALUES ("Rode", 357, "OPM", 1),
		("Kalsada", 400, "OPM", 1);

INSERT INTO songs (song_name,length,genre,album_id)
	VALUES ("Annyeong", 347, "KPOP", 2),
		("Haseyo", 390, "KPOP", 2);

INSERT INTO songs (song_name,length,genre,album_id)
	VALUES ("Pops", 317, "POP", 3),
		("The Pop", 450, "POP", 3);

INSERT INTO songs (song_name,length,genre,album_id)
	VALUES ("12 MN", 457, "SENTI", 4),
		("Nighty", 310, "SENTI", 4);

INSERT INTO songs (song_name,length,genre,album_id)
	VALUES ("New Jeans", 157, "PPOP", 5),
		("Pantalon", 500, "PPOP", 5);

INSERT INTO songs (song_name,length,genre,album_id)
	VALUES ("Beach", 67, "Reggae", 6),
		("Dreads", 200, "Reggae", 6);

INSERT INTO songs (song_name,length,genre,album_id)
	VALUES ("Ching", 267, "CPOP", 7),
		("Chong", 290, "CPOP", 7);


-- [Section] READ data from our database
-- Syntax:
	-- SELECT <column_name> FROM <table_name>;

SELECT * FROM songs;

SELECT song_name FROM songs;

SELECT song_name, genre FROM songs;

SELECT * FROM songs WHERE genre = "OPM";

SELECT * FROM albums WHERE artist_id = 3;

-- We can user AND and OR keyword for multiple expressions in the WHERE clause

SELECT title, length FROM songs WHERE length > 400 AND genre = "OPM";

-- [Section] UPDATING RECORDS/DATA
-- Syntax:
	-- UPDATE <table_name> SET <column_name> = <value_tobe> WHERE <condition>

UPDATE songs SET length = 428 WHERE song_name = "Kundiman";

UPDATE songs SET genre = "Original Pinoy Music" WHERE genre = "OPM";

UPDATE songs SET genre = "Pop" WHERE genre IS NULL;

-- [Section] DELETE RECORDS
-- Syntax:
	-- DELETE FROM <table_name> WHERE <condition>

DELETE FROM songs WHERE genre = "Original Pinoy Music" AND length > 400;

DELETE FROM songs WHERE genre = "PPOP" AND length > 500;